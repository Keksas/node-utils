# Change Log
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [unreleased]

### Changed
### Added

## [1.4.2] - 2017-08-26

### Added
- Process spawner
- `npm bin` equivalent

## [1.4.0] - 2017-06-17

### Changed
- Logging now uses syslog severity levels

## [1.3.1] - 2017-06-10
- Added npm@5 package-lock

## [1.3.0] - 2017-06-10

### Changed
- Updated logging to support tags
- Changed the way error logs are handled
- Made logging less verbose when runnging tests

## [1.2.0] - 2017-04-30
### Added
- `fs.resolvePath`
- `fs.require`

### Changes
- `fs.getModules` renamed to `fs.requireModules`
