## Modules

<dl>
<dt><a href="#module_exec">exec</a></dt>
<dd></dd>
<dt><a href="#module_fs">fs</a></dt>
<dd></dd>
<dt><a href="#module_log">log</a></dt>
<dd></dd>
<dt><a href="#module_path">path</a></dt>
<dd></dd>
<dt><a href="#module_time">time</a></dt>
<dd></dd>
</dl>

<a name="module_exec"></a>

## exec
<a name="exp_module_exec--module.exports"></a>

### module.exports(cmd, args, [out]) ⇒ <code>undefined</code> ⏏
Wrapper for spawning child processes

**Kind**: Exported function  

| Param | Type | Description |
| --- | --- | --- |
| cmd | <code>string</code> | Executable path |
| args | <code>array</code> | Arguments |
| [out] | <code>string</code> | If specified stdout will be redirected to this file |

<a name="module_fs"></a>

## fs

* [fs](#module_fs)
    * [.readdir(dir, options)](#module_fs.readdir) ⇒ <code>Promise.&lt;Array&gt;</code>
    * [.resolvePath(dir, [source])](#module_fs.resolvePath) ⇒ <code>string</code>
    * [.requireModules(dir, [args], [eCallback])](#module_fs.requireModules) ⇒ <code>object</code>
    * [.require(dir, [args], [eCallback])](#module_fs.require) ⇒ <code>object</code>

<a name="module_fs.readdir"></a>

### fs.readdir(dir, options) ⇒ <code>Promise.&lt;Array&gt;</code>
Promisified fs.readdir

**Kind**: static method of [<code>fs</code>](#module_fs)  
**Returns**: <code>Promise.&lt;Array&gt;</code> - - Promise to return a list of files  

| Param | Type | Description |
| --- | --- | --- |
| dir | <code>string</code> \| <code>Buffer</code> | Directory path |
| options | <code>string</code> \| <code>object</code> | Passed to fs.readdir |

<a name="module_fs.resolvePath"></a>

### fs.resolvePath(dir, [source]) ⇒ <code>string</code>
Resolves given string to an absolute path.
If relative path is given uses current working directory.

**Kind**: static method of [<code>fs</code>](#module_fs)  
**Returns**: <code>string</code> - - Resolved path  

| Param | Type | Description |
| --- | --- | --- |
| dir | <code>string</code> | Path to resolve |
| [source] | <code>string</code> | Environment variable SOURCE_PATH can override this |

**Example**  
```js
resolvePath()                // -> '/home/foo/dev/node-utils/src'
resolvePath('lib')           // -> '/home/foo/dev/node-utils/src/lib'
resolvePath('lib', 'server') // -> '/home/foo/dev/node-utils/server/lib'
```
<a name="module_fs.requireModules"></a>

### fs.requireModules(dir, [args], [eCallback]) ⇒ <code>object</code>
Finds and requires all modules in a given directory.
Only tries to load js files and skips index.js

**Kind**: static method of [<code>fs</code>](#module_fs)  
**Returns**: <code>object</code> - - Object of found modules  

| Param | Type | Description |
| --- | --- | --- |
| dir | <code>string</code> | Path to the dir |
| [args] | <code>array.&lt;T&gt;</code> | Arguments passed to the required modules |
| [eCallback] | <code>function</code> | Error callback if readdir fails |

<a name="module_fs.require"></a>

### fs.require(dir, [args], [eCallback]) ⇒ <code>object</code>
Combines {fs.resolvePath} with {fs.requireModules}

**Kind**: static method of [<code>fs</code>](#module_fs)  
**Returns**: <code>object</code> - - Object of found modules  

| Param | Type | Description |
| --- | --- | --- |
| dir | <code>string</code> | Path to the dir |
| [args] | <code>array.&lt;T&gt;</code> | Arguments passed to the required modules |
| [eCallback] | <code>function</code> | Error callback if readdir fails |

<a name="module_log"></a>

## log

* [log](#module_log)
    * [~debug(...args)](#module_log..debug) ⇒ <code>undefined</code>
    * [~info(message, [...args])](#module_log..info) ⇒ <code>undefined</code>
    * [~notice(message, [...args])](#module_log..notice) ⇒ <code>undefined</code>
    * [~warn(message, [...args])](#module_log..warn) ⇒ <code>undefined</code>
    * [~error(message, [...args])](#module_log..error) ⇒ <code>undefined</code>
    * [~critical(message, [...args])](#module_log..critical) ⇒ <code>undefined</code>
    * [~alert(message, [...args])](#module_log..alert) ⇒ <code>undefined</code>
    * [~emergency(message, [...args])](#module_log..emergency) ⇒ <code>undefined</code>

<a name="module_log..debug"></a>

### log~debug(...args) ⇒ <code>undefined</code>
Logs a debug message

**Kind**: inner method of [<code>log</code>](#module_log)  

| Param | Type | Description |
| --- | --- | --- |
| ...args | <code>\*</code> | Arbitrary arguments to be written |

<a name="module_log..info"></a>

### log~info(message, [...args]) ⇒ <code>undefined</code>
Logs an info message

**Kind**: inner method of [<code>log</code>](#module_log)  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message to be written |
| [...args] | <code>\*</code> | Arbitrary arguments to be written |

<a name="module_log..notice"></a>

### log~notice(message, [...args]) ⇒ <code>undefined</code>
Logs a notice

**Kind**: inner method of [<code>log</code>](#module_log)  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message to be written |
| [...args] | <code>\*</code> | Arbitrary arguments to be written |

<a name="module_log..warn"></a>

### log~warn(message, [...args]) ⇒ <code>undefined</code>
Logs a warning message

**Kind**: inner method of [<code>log</code>](#module_log)  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message to be written |
| [...args] | <code>\*</code> | Arbitrary arguments to be written |

<a name="module_log..error"></a>

### log~error(message, [...args]) ⇒ <code>undefined</code>
Logs a info message

**Kind**: inner method of [<code>log</code>](#module_log)  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message to be written |
| [...args] | <code>\*</code> | Arbitrary arguments to be written |

<a name="module_log..critical"></a>

### log~critical(message, [...args]) ⇒ <code>undefined</code>
Logs a critical message

**Kind**: inner method of [<code>log</code>](#module_log)  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message to be written |
| [...args] | <code>\*</code> | Arbitrary arguments to be written |

<a name="module_log..alert"></a>

### log~alert(message, [...args]) ⇒ <code>undefined</code>
Logs an alert

**Kind**: inner method of [<code>log</code>](#module_log)  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message to be written |
| [...args] | <code>\*</code> | Arbitrary arguments to be written |

<a name="module_log..emergency"></a>

### log~emergency(message, [...args]) ⇒ <code>undefined</code>
Logs an emergency

**Kind**: inner method of [<code>log</code>](#module_log)  

| Param | Type | Description |
| --- | --- | --- |
| message | <code>string</code> | Message to be written |
| [...args] | <code>\*</code> | Arbitrary arguments to be written |

<a name="module_path"></a>

## path
<a name="module_path.bin"></a>

### path.bin(name) ⇒ <code>string</code>
Resolve path to npm binary
taking into consideration os being used

**Kind**: static method of [<code>path</code>](#module_path)  
**Returns**: <code>string</code> - Resolved path to binary  

| Param | Type | Description |
| --- | --- | --- |
| name | <code>string</code> | Binary file name |

<a name="module_time"></a>

## time
<a name="module_time.ts"></a>

### time.ts() ⇒ <code>String</code>
Timestamp
Returns UTC time in ISO format (eg 2017-04-27 00:16:11).
This timestamp is meant to be human readable.

**Kind**: static method of [<code>time</code>](#module_time)  
**Returns**: <code>String</code> - UTC timestamp  
