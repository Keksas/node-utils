/**
 * @module exec
 */

const spawn = require('child_process').spawn;
const createWriteStream = require('fs').createWriteStream;

/**
 * Wrapper for spawning child processes
 * @param  {string} cmd  Executable path
 * @param  {array} args Arguments
 * @param  {string} [out] If specified stdout will be redirected to this file
 * @return {undefined}
 */
module.exports = (cmd, args, out = null) => {
  const proc = spawn(cmd, args, {
    cwd: process.cwd(),
    stdio: [
      'inherit',
      out ? 'pipe' : 'inherit',
      'inherit'
    ]
  });

  if (out) {
    const stream = createWriteStream(out);
    proc.stdout.pipe(stream);
  }

  proc.on('close', code => process.exit(code));
};
