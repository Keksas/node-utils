/**
 * @module fs
 */

const fs = require('fs');
const path = require('path');

/**
 * Promisified fs.readdir
 * @param  {string|Buffer} dir     - Directory path
 * @param  {string|object} options - Passed to fs.readdir
 * @return {Promise<Array>}        - Promise to return a list of files
 */
exports.readdir = (dir, options) => (new Promise((resolve, reject) => {
  fs.readdir(dir, options, (err, files) => {
    if (err) {
      return reject(err);
    }
    return resolve(files);
  });
}));

/**
 * Resolves given string to an absolute path.
 * If relative path is given uses current working directory.
 *
 * @example
 * resolvePath()                // -> '/home/foo/dev/node-utils/src'
 * resolvePath('lib')           // -> '/home/foo/dev/node-utils/src/lib'
 * resolvePath('lib', 'server') // -> '/home/foo/dev/node-utils/server/lib'
 * @param  {string} dir      - Path to resolve
 * @param  {string} [source] - Environment variable SOURCE_PATH can override this
 * @return {string}          - Resolved path
 */
exports.resolvePath = (dir, source = 'src') => {
  if (path.isAbsolute(dir)) {
    return dir;
  }

  return path.join(
    process.cwd(),
    process.env.SOURCE_PATH ? process.env.SOURCE_PATH : source,
    dir
  );
};

/**
 * Finds and requires all modules in a given directory.
 * Only tries to load js files and skips index.js
 * @param {string} dir           - Path to the dir
 * @param {array.<T>} [args]     - Arguments passed to the required modules
 * @param {function} [eCallback] - Error callback if readdir fails
 * @return {?object}             - Object of found modules
 */
exports.requireModules = (dir, args = [], eCallback) => {
  let files;
  try {
    files = fs.readdirSync(dir);
  } catch (error) {
    return typeof eCallback === 'function' ? eCallback(error) : null;
  }

  files = files.filter(file => file !== 'index.js');
  files = files.map(file => file.replace('.js', ''));

  const modules = {};
  files.forEach((file) => {
    modules[file] = require(`${dir}/${file}`); // eslint-disable-line global-require, import/no-dynamic-require
    if (args.length > 0) {
      modules[file] = modules[file](...args);
    }
  });

  return modules;
};

/**
 * Combines {fs.resolvePath} with {fs.requireModules}
 * @param {string} dir           - Path to the dir
 * @param {array.<T>} [args]     - Arguments passed to the required modules
 * @param {function} [eCallback] - Error callback if readdir fails
 * @return {?object}             - Object of found modules
 */
exports.require = (dir, args = [], eCallback) =>
  this.requireModules(this.resolvePath(dir), args, eCallback);
