/**
 * @module log
 */

const time = require('./time');

/**
 * Logging levels
 * @private
 * @type {Object}
 */
const LEVEL = {
  EMERGENCY: 'EMERGENCY',
  ALERT: 'ALERT',
  CRITICAL: 'CRITICAL',
  ERROR: 'ERROR',
  WARNING: 'WARNING',
  NOTICE: 'NOTICE',
  INFO: 'INFO',
  DEBUG: 'DEBUG'
};

/**
 * On this environment informational messages are not shown
 * Error and debug messages are still printed
 * @private
 * @type {String}
 */
const SILENT_ENVIRONMENT = process.env.SILENT_ENVIRONMENT || 'test';

/**
 * These log levels are considered errors
 * Their output goes to stderr
 * @private
 * @type {Array}
 */
const ERRORS = [
  LEVEL.EMERGENCY,
  LEVEL.ALERT,
  LEVEL.CRITICAL,
  LEVEL.ERROR
];

/**
 * These levels are logged when running in verbose environment
 * @private
 * @type {Array}
 */
const VERBOSE = [
  LEVEL.INFO,
  LEVEL.NOTICE,
  LEVEL.DEBUG
];

const COLOR_GREEN = '\x1b[92m';
const COLOR_GRAY = '\x1b[38;5;243m';
const COLOR_YELLOW = '\x1b[93m';
const COLOR_RED = '\x1b[31m';
const COLOR_RESET = '\x1b[0m';

const COLOR = {
  [LEVEL.ERROR]: COLOR_RED,
  [LEVEL.WARNING]: COLOR_YELLOW,
  [LEVEL.NOTICE]: COLOR_YELLOW,
  [LEVEL.INFO]: COLOR_GRAY,
  [LEVEL.DEBUG]: COLOR_GREEN
};

/**
 * Formats a string
 * @private
 * @param {string} tag     - Hint for module that logs
 * @param {string} level   - Log level
 * @param {string} message - string to be logged
 * @param {string} color   - TTY color
 * @return {string}        - Colored and timestamped string
 */
const format = (tag, level, message, color = null) => {
  let begin = '';
  let end = '';

  // Apply color only if tty
  if (process.stdout.isTTY && color) {
    begin += color;
    end = COLOR_RESET;
  }

  return `${begin}${time.ts()} ${tag}.${level} ${message}${end}\n`;
};

const printArgument = (arg, isError) => {
  if (isError) {
    return console.error(arg); // eslint-disable-line no-console
  }

  return console.log(arg); // eslint-disable-line no-console
};

/**
 * Writes the message to stdout or stderr
 * Before writing does verbosity checks
 * @private
 * @param {string} tag      - Hint for module that logs
 * @param  {LEVEL} level    - verbosity level
 * @param  {string} message - message to be written
 * @param  {Array} args     - other arguments
 * @return {undefined}
 */
const write = (tag, level, message, args) => {
  if (VERBOSE.includes(level) && process.env.NODE_ENV === SILENT_ENVIRONMENT) {
    return;
  }

  const stream = ERRORS.includes(level) ? 'stderr' : 'stdout';
  const color = COLOR[level];

  process[stream].write(format(tag, level, message, color));

  if (args) {
    args.forEach(arg => printArgument(arg, (stream === 'stderr')));
  }
};

module.exports = function LoggerInterface(tag) {
  return {
    /**
     * Logs a debug message
     * @param  {...*} args - Arbitrary arguments to be written
     * @return {undefined}
     */
    debug: (...args) => {
      const message = /\(([^)]+)\)/.exec((new Error()).stack.split('at ')[2])[1];
      write(tag, LEVEL.DEBUG, message, args);
    },

    /**
     * Logs an info message
     * @param  {string} message - Message to be written
     * @param  {...*} [args]       - Arbitrary arguments to be written
     * @return {undefined}
     */
    info: (message, ...args) => write(tag, LEVEL.INFO, message, args),

    /**
     * Logs a notice
     * @param  {string} message - Message to be written
     * @param  {...*} [args]       - Arbitrary arguments to be written
     * @return {undefined}
     */
    notice: (message, ...args) => write(tag, LEVEL.NOTICE, message, args),

    /**
     * Logs a warning message
     * @param  {string} message - Message to be written
     * @param  {...*} [args]       - Arbitrary arguments to be written
     * @return {undefined}
     */
    warn: (message, ...args) => write(tag, LEVEL.WARNING, message, args),

    /**
     * Logs a info message
     * @param  {string} message - Message to be written
     * @param  {...*} [args]       - Arbitrary arguments to be written
     * @return {undefined}
     */
    error: (message, ...args) => write(tag, LEVEL.ERROR, message, args),

    /**
     * Logs a critical message
     * @param  {string} message - Message to be written
     * @param  {...*} [args]       - Arbitrary arguments to be written
     * @return {undefined}
     */
    critical: (message, ...args) => write(tag, LEVEL.CRITICAL, message, args),

    /**
     * Logs an alert
     * @param  {string} message - Message to be written
     * @param  {...*} [args]       - Arbitrary arguments to be written
     * @return {undefined}
     */
    alert: (message, ...args) => write(tag, LEVEL.ALERT, message, args),

    /**
     * Logs an emergency
     * @param  {string} message - Message to be written
     * @param  {...*} [args]       - Arbitrary arguments to be written
     * @return {undefined}
     */
    emergency: (message, ...args) => write(tag, LEVEL.EMERGENCY, message, args)
  };
};
