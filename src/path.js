/**
 * @module path
 */

const path = require('path');
const os = require('os');

module.exports = {

  /**
   * Resolve path to npm binary
   * taking into consideration os being used
   * @param  {string} name Binary file name
   * @return {string} Resolved path to binary
   */
  bin: (name) => {
    const isWindows = os.platform() === 'win32';
    const binName = isWindows ? `${name}.cmd` : name;
    return path.join('node_modules', '.bin', binName);
  }
};
