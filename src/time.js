/**
 * @module time
 */

/**
 * Timestamp
 * Returns UTC time in ISO format (eg 2017-04-27 00:16:11).
 * This timestamp is meant to be human readable.
 * @return {String} UTC timestamp
 */
exports.ts = () => {
  const date = new Date();
  return date.toISOString().slice(0, 19).replace('T', ' ');
};
