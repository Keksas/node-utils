const fs = require('../src/fs');

describe('fs', () => {
  describe('readdir()', () => {
    it('should return file list', () =>
      expect(fs.readdir('src/')).to.eventually.have.length(4));

    it('should reject reject promise on unexisting directory', () =>
      expect(fs.readdir('foo/')).to.eventually.be.rejected);
  });

  describe('resolvePath()', () => {
    it('should resolve relative path', () =>
      expect(fs.resolvePath('.', '')).to.be.equal(process.cwd()));

    it('should return absolute path', () => {
      expect(fs.resolvePath('/tmp')).to.be.equal('/tmp');
    });
  });
});
