#!/usr/bin/env node

const log = require('../src/log')('SANDBOX');

log.info('Sandbox started');

log.notice('notice');
log.warning('warning');
log.error('error');
log.critical('critical');
log.alert('alert');
log.emergency('emergency');

const err = new Error('test error');
log.error('An error has occured', err);

log.debug({ foo: 'bar' });

log.info('Sandbox success!');
